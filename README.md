
# Ty Cast (PHP Casting Helper)

Php Casting - Convert Operations. Object to object, object to array, array to object, array to array casting.


TyCast paketi; php object ve array değişkenleri arasında birbirlerine tip dönüştürme işlemi yapılır. Amaç, Projeler de bir çok kez yaşadığımız array ile nesne, nesne ile nesne arasında dönüşüm yapmakta sıkça rastladığımız işlemleri pratikleştirme, kolayca her yerde kullanabilme imkanı sağlamak.

## Feature
- Gelistirilebilir yapi
- Composer paket destegi, kolay kurulum, entegre
- Testler
- PSR-4
- Design Patterns

## Support Types: 
- Array To Array,
- Array To Object,
- Object To Object,
- Object To Array
- Array To Array

## Todos
 - PHP Unit Tests

## Requires & Dependency
- PHP Version >= 5.4

## Version
- v1.0.0

## Installation
Download bitbucket or composer
- Bitbucket: https://bitbucket.org/tahsinyuksel/tycast
- Composer Install: composer require tahsinyuksel/ty-cast

Bitbucket


```sh
$ git clone https://tahsinyuksel@bitbucket.org/tahsinyuksel/tycast.git
```


Composer

```sh
$ composer require tahsinyuksel/ty-cast
```

## Examples
- Tests: Test folder in test and example actions

## License
MIT

## Contact
Questions, suggestions, comments:


Tahsin Yüksel
info@tahsinyuksel.com
