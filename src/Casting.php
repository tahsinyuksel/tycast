<?php
namespace TyCast;

class Casting {

	/**
	* $options[
	* 	'matching' = [
	*		'sourceField' => 'targetField'
	* 	]
	* ]
	*/
	function arrayToObject($sourceVar, $targetVar, $options = array()) {

		$matching = isset( $options['matching'] ) && is_array( $options['matching'] ) ? $options['matching'] : array();

		// is matching 
		if(empty($matching)){
			$sourceKeys = array_keys($sourceVar);
			$targetKeys = $sourceKeys;
			$matching = array_combine($sourceKeys, $targetKeys);
		}

		foreach($matching as $sourceKey => $targetKey) {
			
			$value = $sourceVar[$sourceKey];

			$targetSetMethod = 'set' . ucfirst($targetKey);
			// is getter, setter
			if(method_exists($targetVar, $targetSetMethod)) {
				$targetVar->$targetSetMethod($value);
			} else {
				// is object set variable value
				if(isset($targetVar->$targetKey)) {
					$targetVar->$targetKey = $value;	
				}			
			}
		}
		
		return $targetVar;
	}
}