<?php

namespace TyCast\Tests;

class A
{
	private $var1 = 1;
	protected $var2 = 2;
	public $var3 = 3;
	public $var4 = 4;
	
	public function write(){
		echo $this->var1;
	}
}