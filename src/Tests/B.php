<?php

namespace TyCast\Tests;

class B{
	private $var1 = 1;
	protected $var2 = 2;
	public $var3 = 3;
	
	public function write(){
		echo $this->var1;
	}
	
	public function setVar3($val){
		$this->var3 = $val;
	}
}