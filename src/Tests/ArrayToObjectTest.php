<?php

namespace TyCast\Tests;

use TyCast\Tests\B;
use TyCast\Casting;

/**
* Array to object casting operations
*/
class ArrayToObjectTest
{
	public $casting = null;

	public function __construct()
	{
		$this->casting = new Casting();		
	}

	public function convert($sourceVar = array(), $targetVar = array(), $options = array())
	{
		// source array variable
		$arrData = array('exampleKey'=> 'fun style');
		// options matching fields
		$options['matching'] = array('exampleKey'=> 'var3');
		// target object variable
		$b = new B();

		// is external set variable else default set value
		$sourceVar = !empty($sourceVar) ? $sourceVar : $arrData;
		$targetVar = !empty($targetVar) ? $targetVar : $b;
		$options = !empty($options) ? $options : $options;

		// get return target object with source value apply
		$result = $this->casting->arrayToObject($sourceVar, $targetVar, $options);

		return $result;		
	}
}

